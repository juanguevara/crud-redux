import React from "react";
import Products from "./pages/Products";
import NewProduct from "./pages/NewProduct";
import EditProduct from "./pages/EditProduct";
import Home from "./pages/Home";
import { withSimpleHeader } from "./hoc/simpleHeader";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

/**
 * Redux:
 * Importamos Provider que es un componente que recibe como
 * atributo la store , de esta manera centralizas los estados en la app
 */
import { Provider } from "react-redux";
import store from "./store";

function App() {
  return (
    <Router>
      <Provider store={store}>
        <Switch>
          <Route
            path="/"
            exact
            component={Home}
          />
          <Route exact 
            path="/products" 
            component={withSimpleHeader(Products)} />
          <Route
            exact
            path="/products/new"
            component={withSimpleHeader(NewProduct)}
          />
          <Route
            exact
            path="/products/edit/:id"
            component={withSimpleHeader(EditProduct)}
          />
        </Switch>
      </Provider>
    </Router>
  );
}

export default App;
