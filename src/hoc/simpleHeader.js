import React from "react";
import Header from "../components/Header";

//Creamos una cabezera con High Order Component el cual usaremos en las vistas
//fuera del home
export const withSimpleHeader = (Component) => props => (
    <>
      <Header />
      <Component {...props} />
    </>
  );
