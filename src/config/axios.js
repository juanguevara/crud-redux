import axios from 'axios';

const baseURL = 'http://localhost:4000' ;

//Para crear el endpoint
const api = axios.create({
    baseURL: baseURL
});


//Las rutas del endpoint
export const postProducts = data => api.post('/products',data);

export const getProducts = data => api.get('/products',data);

export const deleteProducts = data => api.delete(`/products/${data}`);

export const putProducts = (id,data) => api.put(`/products/${id}`,data);