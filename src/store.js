import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import reducer from "./redux/reducers";

//Creamos una función para guardar el estado en el localStorage
function saveToLocalStorage(state) {
  try {
    const serializedState = JSON.stringify(state)
    localStorage.setItem('state', serializedState)
  } catch(e) {
    console.log(e)
  }
}

//Creamos la funciona que carga esa data para posteriormente pasarla al store
function loadFromLocalStorage() {
  try {
    const serializedState = localStorage.getItem('state')
    if (serializedState === null) return undefined
    return JSON.parse(serializedState)
  } catch(e) {
    console.log(e)
    return undefined
  }
}

const persistedState = loadFromLocalStorage();

/**
 * createStore es el método para crear la store que recibe como parametro el o los estados.
 * En este caso recibe reducer que son varios estados combinados con el metodo combineReducer.
 * En el segundo parametro va a recibir la persistencia del estado. En el tercer parametro
 * parámetro va a recibir un compose que se encarga de componer 2 funciones, para este caso,
 * applyMiddleware se va a encargar de aplicar thunk que nos va a permitir
 * ejecutar acciones de manera asíncrona y segundo parametro insertar la
 * extensión redux devtools en la store( Herramienta del navegador que nis
 * va a permitir ver la gestion del store entre otras cosas)
 */
const store = createStore(
  reducer,
  persistedState,
  compose(
    applyMiddleware(thunk),
    typeof window === "object" &&
      typeof window.__REDUX_DEVTOOLS_EXTENSION__ !== "undefined"
      ? window.__REDUX_DEVTOOLS_EXTENSION__()
      : f => f
  )
);

store.subscribe(()=> saveToLocalStorage(store.getState()))

export default store;
