import React from "react";
import { Link, useHistory } from "react-router-dom";
import Swal from "sweetalert2";

//redux
import { useDispatch } from "react-redux";
import {
  borrarProducto,
  editForm
} from "../redux/actions/productActions";

const Product = ({ product }) => {
  //Obtenemos los valores con destrocturing
  const { id, name, price, quantity } = product;

  const history = useHistory(); // Habilitar history para la redirección
  const dispatch = useDispatch();

  //Metodo para eliminar producto
  const deleteProduct = id => {
    //confirmar si desea borrar el producto
    Swal.fire({
      title: "Estas seguro de eliminar el producto?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Confirmar",
      cancelButtonText: "Cancelar"
    }).then(result => {
      if (result.isConfirmed) {
        //Dispatch de la acción de eliminar producto
        dispatch(borrarProducto(id));
      }
    });
  };

  //función que redirige de forma programada
  const redirectEdit = product => {
    dispatch(editForm(product));
    history.push(`/products/edit/${product.id}`);
  };
  return (
    <tbody>
      <tr key={id}>
        <td>{name}</td>
        <td>
          <span className="font-weight-bold">$ {price}</span>
        </td>
        <td>{quantity}</td>
        <td className="acciones">
          <button
            onClick={() => redirectEdit(product)}
            className="btn btn-primary mr-2"
          >
            {" "}
            Editar
          </button>
          <button
            type="button"
            className="btn btn-danger"
            onClick={() => deleteProduct(id)}
          >
            Eliminar
          </button>
        </td>
      </tr>
    </tbody>
  );
};

export default Product;
