import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import Git from "../assets/gitlab-brands.svg";
import Linkedin from "../assets/linkedin-brands.svg";
import Laptop from "../assets/laptop-code-solid.svg"

const Home = () => {
  return (
    <Fragment>
      <div className="bg-primary w-100 h-100 position-absolute d-flex justify-content-center align-items-center">
        {/* <div className="container text-center w-50">
          <h1 className="text-white bg-dark">
            CRUD - React, Redux, REST API(JSON Server) & Axios
          </h1>
        </div> */}
        <div className="jumbotron bg-secondary w-50 py-4">
          <h3 className="text-center font-weight-bold text-white">
            Lista de productos
          </h3>
          <hr className="my-4" />
          <h4 className="font-weight-bold text-white text-center">
            CRUD de una lista de productos usando <br /> React, Redux, REST
            API(JSON Server) & Axios
          </h4>
          <p class="lead text-center">
            <Link
              class="btn btn-primary btn-lg mt-4"
              to="/products"
              role="button"
            >
              ¡Vamos!
            </Link>
          </p>
        </div>
        <div className="fixed-bottom d-flex ml-2">
          <h3 className="text-white font-weight-bold ">By:</h3>
          <h2 className="ml-2 text-white font-weight-bold ">
            <em>Juan Ortega</em>
          </h2>
          <Link className="rrss-icon ml-3" role="button" to="//linkedin.com/in/juan-ortega-066400151/" >
            <img src={Linkedin} alt="linkedin-icon" />
          </Link>
          <Link className="rrss-icon ml-3" role="button" to="#" >
            <img src={Git}  alt="Git-icon" />
          </Link>
          <Link className="rrss-icon ml-3" role="button" to="#" >
            <img src={Laptop}  alt="Laptopt-icon" />
          </Link>
        </div>
      </div>
    </Fragment>
  );
};

export default Home;
