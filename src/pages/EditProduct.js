import React, { useState, useEffect } from "react";
import { useHistory, Link } from "react-router-dom";

import returnArrow from "../assets/angle-left-solid.svg";

//redux
import { useDispatch, useSelector } from "react-redux";
import { editProductsSelector, productsSelector } from "../redux/selectors/productsSelector";
import { editProduct } from "../redux/actions/productActions";

const EditProduct = () => {
  //nuevo state de producto en el componente
  const [product, setProduct] = useState({
    name: "",
    price: "",
    quantity: ""
  });

  //accedemos al estado del producto a editar
  const editableProduct = useSelector(editProductsSelector);

  const history = useHistory();

  const dispatch = useDispatch();

  //producto a editar
  useEffect(() => {
    setProduct(editableProduct);
  }, [editableProduct]);
   
  const { name, price, quantity } = product;

    //Redirección si no encuentramos ninguún producto
    if(!product) {
      return history.push("/products");
     }
  //manejador del evento onChange para obtener los nuevos datos
  const handleChange = e => {
    setProduct({
      ...product,
      [e.target.name]: e.target.value
    });
  };

  const editProductSubmit = e => {
    e.preventDefault();
    //Enviamos la edición del producto para que se guarde en la api
    dispatch(editProduct(product));

    //redireccionamos a la página principal
    history.push("/products");
  };

  return (
    <div className="container mt-5">
      <div className="row justify-content-center">
        <div className="col-md-8">
          <div className="card">
            <div className="card-body position-relative">
              <Link to="/products" role="button">
                <img
                  alt="return-arrow"
                  src={returnArrow}
                  className="return-arrow-icon"
                />
              </Link>

              <h2 className="text-center mb-4 font-weight-bold ">
                Editar Producto
              </h2>

              <form onSubmit={editProductSubmit}>
                <div className="form-group">
                  <label>Nombre de Producto</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Ej: Zapatos, Libros, etc"
                    name="name"
                    value={name}
                    onChange={handleChange}
                  />
                </div>

                <div className="form-group">
                  <label>Precio de Producto</label>
                  <input
                    type="number"
                    className="form-control"
                    placeholder="0"
                    name="price"
                    value={price}
                    onChange={handleChange}
                  />
                </div>

                <div className="form-group">
                  <label>Cantidad</label>
                  <input
                    type="number"
                    className="form-control"
                    placeholder="Cantidad"
                    name="quantity"
                    min="1"
                    value={quantity}
                    onChange={handleChange}
                  />
                </div>

                <button
                  type="submit"
                  className="btn btn-primary 
                  font-weight-bold text-uppercase
                  d-block w-100 "
                >
                  Guardar Cambios
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditProduct;
