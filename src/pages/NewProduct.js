import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";

import returnArrow from "../assets/angle-left-solid.svg";

//importamos un disparador de acciones
import { creatingNewProduct } from "../redux/actions/productActions";
import { createAlert, hideAlert } from "../redux/actions/alertActions";

//importamos el selector del state que vamos a acceder
import { stateSelector } from "../redux/selectors/productsSelector";
import { stateAlertSelector } from "../redux/selectors/alertaSelector";

//utils
import { formAlert as alertMsg } from "../utils/alertsText";

/**
 * Componente que crea la vista para agregar nuevos productos.
 * @param {Function} history Cuando nuestros componentes se encuentran en el routing
 *                           podemos acceder al history del enrutador, lo cual nos
 *                           puede funcionar para redireccionar a cualquier parte en la app
 */
const NewProduct = ({ history }) => {
  //estado del componente
  const [product, setProduct] = useState({
    name: "",
    price: 0,
    quantity: 0
  });

  //El metodo para manejar los cambios en los input y añadirlos al objeto del estado
  const handleChange = e => {
    setProduct(
      prevProduct => (
        e.target.name === "price" && Number(e.target.value),
        {
          ...prevProduct,
          [e.target.name]: e.target.value
        }
      )
    );
  };

  //useDispatch te crea una función para acceder a las acciones del redux
  const dispatch = useDispatch();

  //Accede al state del store
  const state = useSelector(stateSelector);
  const alertState = useSelector(stateAlertSelector);

  //Llama al disparador de acciones en productoActions
  const addProduct = product => dispatch(creatingNewProduct(product));

  //cuando el usuario haga submit se ejecuta la acción
  const submitNuevoProducto = e => {
    e.preventDefault();

    //validar formulario
    if (
      product.name.trim() === "" ||
      product.price <= 0 ||
      product.quantity <= 0
    ) {
      dispatch(createAlert(alertMsg));
      return;
    }

    dispatch(hideAlert());

    // crear el nuevo producto
    addProduct(product);

    //redireccionar
    if (state.products.length > 0) {
      setTimeout(() => history.push("/products"), 500);
    }
  };

  return (
    <div className="container mt-5">
      <div className="row justify-content-center">
        <div className="col-md-8">
          <div className="card">
            <div className="card-body position-relative">
              <h2 className="text-center mb-4 font-weight-bold">
                Agregar Nuevo Producto
              </h2>
              <Link to="/products" role="button">
                <img
                  alt="return-arrow"
                  src={returnArrow}
                  className="return-arrow-icon"
                />
              </Link>
              {alertState !== null ? (
                <p className={alertState.classes}>{alertState.msg}</p>
              ) : null}
              <form onSubmit={submitNuevoProducto}>
                <div className="form-group">
                  <label>Nombre de Producto</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Ej: Zapatos, Libros, etc"
                    name="name"
                    value={product.name}
                    onChange={handleChange}
                  />
                </div>

                <div className="form-group">
                  <label>Precio de Producto</label>
                  <input
                    type="number"
                    className="form-control"
                    placeholder="Precio"
                    name="price"
                    value={product.price}
                    onChange={handleChange}
                  />
                </div>

                <div className="form-group">
                  <label>Cantidad</label>
                  <input
                    type="number"
                    className="form-control"
                    placeholder="Cantidad"
                    name="quantity"
                    value={product.quantity}
                    onChange={handleChange}
                  />
                </div>

                <button
                  type="submit"
                  className="btn btn-primary 
                    font-weight-bold text-uppercase
                    d-block w-100 "
                >
                  Agregar
                </button>
              </form>
              {state.loading ? (
                <p className="text-center text-uppercase text-dark p2 mt-4">
                  Cargando...
                </p>
              ) : null}
              {state.error ? (
                <p className="alert alert-danger p2 mt-4 text-center">
                  Hubo un error
                </p>
              ) : null}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewProduct;
