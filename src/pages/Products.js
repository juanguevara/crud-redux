import React, { Fragment, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Product from "../components/Product";

//importamos un disparador de acciones
import { getingProducts } from "../redux/actions/productActions";

//importamos el selector del state que vamos a acceder
import {
  sortedProductsSelector,
  productsSelectorError,
  productsSelectorLoading
} from "../redux/selectors/productsSelector";

const Products = () => {
  const [filter, setFilter] = useState(null);
  //useDispatch te crea una función para acceder a las acciones del redux
  const dispatch = useDispatch();

  //Accede a los productos del store ordenadamente
  const products = useSelector(state => sortedProductsSelector(state, filter));

  //Llamamos al useEffect para que no genere un render
  //infinito al traer los productos al renderizar el componente
  useEffect(() => {
    //activas la accion para obtener los productos
    const productList = () => dispatch(getingProducts());
    //Hacemos el llamado a la acción para que llame los productos al entrar al componente
    productList();
  }, []);

  //accede a los errores del store
  const error = useSelector(productsSelectorError);

  //precio total del listado
  const totalPrice = products => {
    const total = products.reduce(
      (total, add) => total + Number(add.price) * Number(add.quantity),
      0);

      return Number(total.toFixed(2))
  };

  //accede a el estado de si esta cargando del store
  const loading = useSelector(productsSelectorLoading);

  const selectFilter = filterName => {
    if (filterName === filter) {
      return setFilter(null);
    }

    setFilter(filterName);
  };

  return (
    <Fragment>
      <div className="container mt-5">
        <h2 className="text-center my-5">Listado de Productos</h2>

        {error && (
          <p className="font-weight-bold alert alert-danger text-center mt-4">
            Hubo un error
          </p>
        )}
        {loading && <p className="text-center font-weight-bold">Cargando</p>}

        <table className="table table-striped">
          <thead className="bg-primary table-dark">
            <tr>
              <th
                scope="col"
                name="name"
                onClick={() => selectFilter("name")}
                className="click"
              >
                Nombre
              </th>
              <th
                scope="col"
                name="price"
                onClick={() => selectFilter("price")}
                className="click"
              >
                Precio
              </th>
              <th
                scope="col"
                name="quantity"
                onClick={() => selectFilter("quantity")}
                className="click"
              >
                Cantidad
              </th>
              <th scope="col">Acciones</th>
            </tr>
          </thead>
          {!products.length
            ? "No hay productos"
            : products.map(product => {
                return <Product key={product.id} product={product} />;
              })}
        </table>
        <p className="font-weight-bold text-primary pl-2 my-4">
          Total precio productos:&nbsp;
          {totalPrice(products)}
        </p>
      </div>
    </Fragment>
  );
};

export default Products;
