//Función para ordenar de manera ascendente los datos obtenidos
export const sortProductList = (filterName, product) => {
  const filterValue = filterName || 'id';

  if (filterValue === "name") {
    return product.sort((a, b) =>
      a[filterValue] > b[filterValue]
        ? 1
        : b[filterValue] > a[filterValue]
        ? -1
        : 0
    );
  }

  return product.sort((a, b) => a[filterValue] - b[filterValue]);
};
