//llamas el tipo de acción
import {
  ADD_PRODUCT,
  ADD_PRODUCT_SUCCES,
  ADD_PRODUCT_ERROR,
  GET_PRODUCT,
  GET_PRODUCT_SUCCESS,
  GET_PRODUCT_ERROR,
  REMOVE_PRODUCT,
  REMOVE_PRODUCT_ERROR,
  REMOVE_PRODUCT_SUCCESS,
  EDIT_PRODUCT_ERROR,
  EDIT_PRODUCT_SUCCESS,
  FORM_PRODUCT
} from "../types";

// Cada reducer tiene su estado
const initialState = {
  products: [],
  productToRemove: null,
  productToEdit: null,
  error: false,
  loading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_PRODUCT:
    case ADD_PRODUCT:
      return {
        ...state,
        loading: action.payload
      };
    case ADD_PRODUCT_SUCCES:
      return {
        ...state,
        loading: false,
        products: [...state.products, action.payload]
      };
    case GET_PRODUCT_ERROR:
    case ADD_PRODUCT_ERROR:
    case REMOVE_PRODUCT_ERROR:
    case EDIT_PRODUCT_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload
      };

    case GET_PRODUCT_SUCCESS:
      return {
        ...state,
        loading: false,
        products: action.payload
      };
    case REMOVE_PRODUCT:
      return {
        ...state,
        productToRemove: action.payload
      };
    case REMOVE_PRODUCT_SUCCESS:
      return {
        ...state,
        products: state.products.filter(
          product => product.id !== state.productToRemove
        ),
        productToRemove: null
      };
    case FORM_PRODUCT:
      return {
        ...state,
        productToEdit: action.payload
      };
    case EDIT_PRODUCT_SUCCESS:
      return {
        ...state,
        productToEdit: null,
        products: state.products.map(product =>
          product.id === action.payload.id
            ? (product = action.payload)
            : product
        )
      };
    default:
      return state;
  }
}
