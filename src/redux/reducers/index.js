import { combineReducers } from "redux";
import productsReducer from "./productsReducer";
import alertReducer from "./alertReducer"

//Como su nombre lo indica, combina varios reducer para juntar los estados
//en un solo reducer
export default combineReducers({
  products: productsReducer,
  alert: alertReducer
});
