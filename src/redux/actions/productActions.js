//llamas el tipo de acción
import {
  ADD_PRODUCT,
  ADD_PRODUCT_SUCCES,
  ADD_PRODUCT_ERROR,
  GET_PRODUCT,
  GET_PRODUCT_SUCCESS,
  GET_PRODUCT_ERROR,
  REMOVE_PRODUCT,
  REMOVE_PRODUCT_SUCCESS,
  REMOVE_PRODUCT_ERROR,
  FORM_PRODUCT,
  EDIT_PRODUCT,
  EDIT_PRODUCT_ERROR,
  EDIT_PRODUCT_SUCCESS,
} from "../types";
import {
  postProducts,
  getProducts,
  deleteProducts,
  putProducts
} from "../../config/axios";
import Swal from "sweetalert2";

/**
 * Por motivos de rapidez en el ejercicio colocamos los metodos CRUD en este archivo,
 * sin embargo probablemente, dependiendo del proyecto,  estos metodos tengan una mejor organización
 * si se le asigna un espacio dentro del redux.
 * Un ejemplo de estructura de carpeta podría ser:
 *
 * src ˅
 *  components ˅
 *  config / api ˅
 *  redux ˅
 *      ejemploDeEstado1 ˅
 *          callsApi˅
 *              ejemploDeEstado1CreateEstado1.js
 *              ejemploDeEstado1DeleteEstado1.js
 *              ejemploDeEstado1UpdateEstado1.js
 *              ejemploDeEstado1GetEstado1.js
 *          ejemploDeEstado1Actions.js
 *          ejemploDeEstado1Types.js
 *          ejemploDeEstado1Reducer.js
 *      ejemploDeEstado2 ˅
 *          callsApi˅
 *              ejemploDeEstado1CreateEstado2.js
 *              ejemploDeEstado1DeleteEstado2.js
 *              ejemploDeEstado1UpdateEstado2.js
 *              ejemploDeEstado1GetEstado2.js
 *          ejemploDeEstado2Actions.js
 *          ejemploDeEstado2Types.js
 *          ejemploDeEstado2Reducer.js
 *
 *       ...
 *
 */

//Crear nuevo productos
export const creatingNewProduct = payload => {
  //Para que funcione la promesa asíncrona tenenmos que declarar el async await
  //asi podra realizar varias funciones como recibir el error
  //o realizar otr
  return async dispatch => {
    dispatch(addProduct());

    try {
      //insertar en la API(JSON Server)
      await postProducts(payload);

      //si todo sale bien, actualiza el state
      dispatch(addProductSucces(payload));

      //alerta
      Swal.fire("Correcto", "El producto se agregó correctamente", "success");
    } catch (error) {
      //si hay un error cambiar el state
      dispatch(addProductError(console.log("error ", error)));

      //alerta de error
      Swal.fire({
        icon: "error",
        title: "hubo un error",
        text: "Hubo un error, intenta de nuevo"
      });
    }
  };
};

//inicia la accion de agregar productos
const addProduct = () => ({
  type: ADD_PRODUCT,
  payload: true
});

//si el producto se guarda en la base de datos
const addProductSucces = payload => ({
  type: ADD_PRODUCT_SUCCES,
  payload
});

//si hubo un error
const addProductError = () => ({
  type: ADD_PRODUCT_ERROR,
  payload: true
});

//OBTENER PRODUCTOS
export const getingProducts = () => {
  return async dispatch => {
    //iniciamos la accion de descargar productos
    dispatch(download_product());

    try {
      //recibir la data del API(JSON Server)
      const res = await getProducts();

      //si todo sale bien, actualiza el state
      dispatch(download_product_succes(res.data));
    } catch (error) {
      //si hay un error cambiar el state
      dispatch(download_product_error(console.log("error ", error)));

      //alerta de error
      Swal.fire({
        icon: "error",
        title: "hubo un error",
        text: "Hubo un error, intenta de nuevo"
      });
    }
  };
};

//Inicia la accion de traer productos
const download_product = () => ({
  type: GET_PRODUCT,
  payload: true
});

//Si logramos traer el producto
const download_product_succes = payload => ({
  type: GET_PRODUCT_SUCCESS,
  payload
});

//si hubo un error
const download_product_error = () => ({
  type: GET_PRODUCT_ERROR,
  payload: true
});

// SELECIONA Y ELIMINA EL PRODUCTO
export const borrarProducto = id => {
  return async dispatch => {
    dispatch(removeProduct(id));

    try {
      await deleteProducts(id);
      dispatch(removeProductSucces());
      Swal.fire(
        "Eliminado!",
        "El producto se ha eliminado con éxito",
        "success"
      );
    } catch (error) {
      dispatch(removeProductError());
    }
  };
};

const removeProduct = id => ({
  type: REMOVE_PRODUCT,
  payload: id
});

const removeProductSucces = () => ({
  type: REMOVE_PRODUCT_SUCCESS
});

const removeProductError = () => ({
  type: REMOVE_PRODUCT_ERROR,
  payload: true
});

//Añade la data al formulario de edición
export const editForm = payload => {
  return dispatch => {
    dispatch(editableProduct(payload));
  };
};

//EDITA UN PRODUCTO
export const editProduct = payload => {
  return async dispatch => {
    dispatch(editingProduct());

    try {
      //editamos el producto en la api
      await putProducts(payload.id, payload);

      //cambiamos el estado del redux
      dispatch(editingProductSuccess(payload));
    } catch (error) {
      console.log(error);
      dispatch(editingProductError());
    }
  };
};

const editableProduct = product => ({
  type: FORM_PRODUCT,
  payload: product
});

const editingProduct = () => ({
  type: EDIT_PRODUCT
});

const editingProductSuccess = product => ({
  type: EDIT_PRODUCT_SUCCESS,
  payload: product
});

const editingProductError = () => ({
  type: EDIT_PRODUCT_ERROR,
  payload: true
});
