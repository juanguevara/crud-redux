import {
    SHOW_ALERT,
    HIDE_ALERT
} from '../types'

//muestra alerta
export const createAlert = payload => ({
    type: SHOW_ALERT,
    payload
})

//oculta alerta
export const hideAlert = () => ({
    type: HIDE_ALERT
})