import { sortProductList } from "../../utils/sortProducts";

export const stateSelector = state => state.products || {};

export const productsSelector = state => stateSelector(state).products || [];

export const sortedProductsSelector = (state, filter) => {
  const sortedProducts = sortProductList(filter, productsSelector(state));
  return sortedProducts;
};
export const editProductsSelector = state =>
  stateSelector(state).productToEdit;

export const productsSelectorError = state => stateSelector(state).error;

export const productsSelectorLoading = state => stateSelector(state).loading;
