# README

CRUD of a shopping list made with **React**,**Redux**,**Json Server** and **Axios**

## Npm install

Do **npm i** or **npm install** in a terminal to download all dependencies and libraries of the project. 

- redux
- react-redux
- redux-thunk
- json-server
- react-router-dom


## Start Project
In a terminal **json-server db.json --port 4000** to initiate json-server(the API) in port 4000.

In another terminal **npm start** to run the project(**in port 3000 by default**)


## Project made by Juan Ortega

[Linkedin Profile](https://www.linkedin.com/in/juan-ortega-066400151/)